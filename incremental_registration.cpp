/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2010, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 *
 */

/* \author Radu Bogdan Rusu
 * adaptation Raphael Favier
 * readaptation Daniele Fiorenti
 * */

#include <boost/make_shared.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>

#include <pcl/io/pcd_io.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>

#include <pcl/features/normal_3d.h>

#include <pcl/registration/icp.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>

#include <pcl/visualization/pcl_visualizer.h>

#include <pcl/common/common_headers.h>

#include <vtkSmartPointer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkActor.h>

using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;

//convenient typedefs
typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;
typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;

const double MAX_DIST  = 0.5;  // orig 0.1
const double MRAND_MIN  = -MAX_DIST*0.3;
const double MRAND_MAX  =  MAX_DIST*0.3;
const double MRAND_AMIN  = -3; //in deg
const double MRAND_AMAX  =  3;

// Global variable for our visualizer
pcl::visualization::PCLVisualizer *p;
// its left and right viewports
int vp_1, vp_2;

//convenient structure to handle our pointclouds
struct PCD
{
    PointCloud::Ptr cloud;
    std::string f_name;
    Eigen::Matrix4f Ti;

    PCD() : cloud (new PointCloud) {};
};

struct PCDComparator
{
    bool operator () (const PCD& p1, const PCD& p2)
    {
        return (p1.f_name < p2.f_name);
    }
};


// Define a new point representation for < x, y, z, curvature >
class MyPointRepresentation : public pcl::PointRepresentation <PointNormalT>
{
    using pcl::PointRepresentation<PointNormalT>::nr_dimensions_;
public:
    MyPointRepresentation ()
    {
        // Define the number of dimensions
        nr_dimensions_ = 4;
    }

    // Override the copyToFloatArray method to define our feature vector
    virtual void copyToFloatArray (const PointNormalT &p, float * out) const
    {
        // < x, y, z, curvature >
        out[0] = p.x;
        out[1] = p.y;
        out[2] = p.z;
        out[3] = p.curvature;
    }
};


////////////////////////////////////////////////////////////////////////////////
/** \brief Display source and target on the first viewport of the visualizer
 *
 */
void showCloudsLeft(const PCD cloud_target, const PCD cloud_source)
{
    p->removeAllPointClouds();

    PointCloudColorHandlerCustom<PointT> tgt_h (cloud_target.cloud, 0, 255, 0); //verde
    PointCloudColorHandlerCustom<PointT> src_h (cloud_source.cloud, 255, 0, 0); //rosso
    p->addPointCloud (cloud_target.cloud, tgt_h, cloud_target.f_name, vp_1);
    p->addPointCloud (cloud_source.cloud, src_h, cloud_source.f_name, vp_1);

    // std::cout << "cloud_target: " << cloud_target.f_name << " is Green" << std::endl;
    // std::cout << "cloud_source: " << cloud_source.f_name << " is Red" << std::endl;

    // The Green cloud can be moved:
    p->getCloudActorMap()->at(cloud_target.f_name).actor->PickableOn();
    // The Red cloud can't be moved:
    p->getCloudActorMap()->at(cloud_source.f_name).actor->PickableOff();

    PCL_INFO ("Press q to begin the registration.\n");
    p-> spin();
}


////////////////////////////////////////////////////////////////////////////////
/** \brief Display source and target on the second viewport of the visualizer
 *
 */
void showCloudsRight(const PointCloudWithNormals::Ptr cloud_target, const PointCloudWithNormals::Ptr cloud_source)
{
    p->removePointCloud ("source");
    p->removePointCloud ("target");


    PointCloudColorHandlerGenericField<PointNormalT> tgt_color_handler (cloud_target, "curvature");
    if (!tgt_color_handler.isCapable ())
        PCL_WARN ("Cannot create curvature color handler!");

    PointCloudColorHandlerGenericField<PointNormalT> src_color_handler (cloud_source, "curvature");
    if (!src_color_handler.isCapable ())
        PCL_WARN ("Cannot create curvature color handler!");


    p->addPointCloud (cloud_target, tgt_color_handler, "target", vp_2);
    p->addPointCloud (cloud_source, src_color_handler, "source", vp_2);

    //Clouds can't be moved
    p->getCloudActorMap()->at("target").actor->PickableOff();
    p->getCloudActorMap()->at("source").actor->PickableOff();

    p->spinOnce();
}

////////////////////////////////////////////////////////////////////////////////
/** \brief Load a set of PCD files that we want to register together
  * \param argc the number of arguments (pass from main ())
  * \param argv the actual command line arguments (pass from main ())
  * \param models the resultant vector of point cloud datasets
  */
void loadData (int argc, char **argv, std::vector<PCD, Eigen::aligned_allocator<PCD> > &models)
{
    std::string extension (".pcd");
    // Suppose the first argument is the actual test model
    for (int i = 1; i < argc; i++)
    {
        std::string fname = std::string (argv[i]);
        // Needs to be at least 5: .plot
        if (fname.size () <= extension.size ())
            continue;

        std::transform (fname.begin (), fname.end (), fname.begin (), (int(*)(int))tolower);

        //check that the argument is a pcd file
        if (fname.compare (fname.size () - extension.size (), extension.size (), extension) == 0)
        {
            // Load the cloud and saves it into the global list of models
            PCD m;
            m.f_name = argv[i];

            // std::cout << "argv in loadData: " << std::endl;
            // std::cout << argv[i] << std::endl;

            pcl::io::loadPCDFile (argv[i], *m.cloud);
            //remove NAN points from the cloud
            std::vector<int> indices;
            pcl::removeNaNFromPointCloud(*m.cloud,*m.cloud, indices);

            models.push_back (m);
        }
    }
}


////////////////////////////////////////////////////////////////////////////////
/** \brief Align a pair of PointCloud datasets and return the result
  * \param cloud_src the source PointCloud
  * \param cloud_tgt the target PointCloud
  * \param output the resultant aligned source PointCloud
  * \param final_transform the resultant transform between source and target
  */
void pairAlign (const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, PointCloud::Ptr output, Eigen::Matrix4f &final_transform, double leaf_size, bool downsample = true)
{
    //
    // Downsample for consistency and speed
    // \note enable this for large datasets
    PointCloud::Ptr src (new PointCloud);
    PointCloud::Ptr tgt (new PointCloud);
    pcl::VoxelGrid<PointT> grid;
    if (downsample)
    {
        grid.setLeafSize (leaf_size, leaf_size, leaf_size);
        grid.setInputCloud (cloud_src);
        grid.filter (*src);

        grid.setInputCloud (cloud_tgt);
        grid.filter (*tgt);

        PCL_INFO ("subsampled to %d and %d points.\n", src->size(), tgt->size ());
    }
    else
    {
        src = cloud_src;
        tgt = cloud_tgt;
    }


    // Compute surface normals and curvature
    PointCloudWithNormals::Ptr points_with_normals_src (new PointCloudWithNormals);
    PointCloudWithNormals::Ptr points_with_normals_tgt (new PointCloudWithNormals);

    pcl::NormalEstimation<PointT, PointNormalT> norm_est;
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
    norm_est.setSearchMethod (tree);
    norm_est.setKSearch (30);

    norm_est.setInputCloud (src);
    norm_est.compute (*points_with_normals_src);
    pcl::copyPointCloud (*src, *points_with_normals_src);

    norm_est.setInputCloud (tgt);
    norm_est.compute (*points_with_normals_tgt);
    pcl::copyPointCloud (*tgt, *points_with_normals_tgt);

    //
    // Instantiate our custom point representation (defined above) ...
    MyPointRepresentation point_representation;
    // ... and weight the 'curvature' dimension so that it is balanced against x, y, and z
    float alpha[4] = {1.0, 1.0, 1.0, 1.0};
    point_representation.setRescaleValues (alpha);

    //
    // Align
#define GICP
#define STANDARD_ICP

#ifdef GICP
    pcl::GeneralizedIterativeClosestPoint<PointNormalT, PointNormalT> reg;
#else
#ifndef STANDARD_ICP
    pcl::IterativeClosestPointNonLinear<PointNormalT, PointNormalT> reg;
#else
    pcl::IterativeClosestPoint<PointNormalT, PointNormalT> reg;
#endif
#endif
    reg.setTransformationEpsilon (1e-16);//provo fino a 1e-16
    // Set the maximum distance between two correspondences (src<->tgt) to 10cm
    // Note: adjust this based on the size of your datasets
    reg.setMaxCorrespondenceDistance (MAX_DIST);
    // Set the point representation
    reg.setPointRepresentation (boost::make_shared<const MyPointRepresentation> (point_representation));

    reg.setInputSource (points_with_normals_src);
    reg.setInputTarget (points_with_normals_tgt);

    // Run the same optimization in a loop and visualize the results
    Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), prev, targetToSource;
    PointCloudWithNormals::Ptr reg_result = points_with_normals_src;
    reg.setMaximumIterations (20); //provato 20
    for (int i = 0; i < 30; ++i)
    {
        PCL_INFO ("Iteration Nr. %d.\n", i);

        // save cloud for visualization purpose
        points_with_normals_src = reg_result;

        // Estimate
        reg.setInputSource (points_with_normals_src);
        reg.align (*reg_result);
#ifdef GICP
        pcl::transformPointCloud (*reg_result, *reg_result, reg.getFinalTransformation ());
#endif

        // accumulate transformation between each Iteration
        Ti = reg.getFinalTransformation () * Ti;

        // if the difference between this transformation and the previous one
        // is smaller than the threshold, refine the process by reducing
        // the maximal correspondence distance
        if (fabs ((reg.getLastIncrementalTransformation () - prev).sum ()) < reg.getTransformationEpsilon ()){
            reg.setMaxCorrespondenceDistance (0.9 * reg.getMaxCorrespondenceDistance ());
            std::cout << "change max distance to "  << reg.getMaxCorrespondenceDistance () << std::endl;
        }

        prev = reg.getLastIncrementalTransformation ();

        // visualize current state
        showCloudsRight(points_with_normals_tgt, points_with_normals_src);
    }
    std::cout << "has converged:" << reg.hasConverged() << " score: " <<
                 reg.getFitnessScore() << std::endl;
    //
    // Get the transformation from target to source
    targetToSource = Ti.inverse();

    //
    // Transform target back in source frame
    pcl::transformPointCloud (*cloud_tgt, *output, targetToSource);

    PCL_INFO ("Press q to show the final result on the original cloud.\n");
    p->spin ();


    p->removePointCloud ("source");
    p->removePointCloud ("target");

    PointCloudColorHandlerCustom<PointT> cloud_tgt_h (output, 0, 255, 0);
    PointCloudColorHandlerCustom<PointT> cloud_src_h (cloud_src, 255, 0, 0);
    p->addPointCloud (output, cloud_tgt_h, "target", vp_2);
    // Not pickable
    p->getCloudActorMap()->at("target").actor->PickableOff();
    p->addPointCloud (cloud_src, cloud_src_h, "source", vp_2);
    // Not pickable
    p->getCloudActorMap()->at("source").actor->PickableOff();
    PCL_INFO ("Press q to continue the registration.\n");
    p->spin ();

    p->removePointCloud ("source");
    p->removePointCloud ("target");

    // add the source to the transformed target
    *output += *cloud_src;

    final_transform = targetToSource;
    std::cout << "final_transform: " << std::endl;
    std::cout << final_transform << std::endl;
}

/* ---[ */
int main (int argc, char** argv)
{
    // Load data
    std::vector<PCD, Eigen::aligned_allocator<PCD> > data;

    double leaf_size = 0.1;
    bool downsample = true;

    loadData (argc, argv, data);

    // Check user input
    if (data.empty ())
    {
        PCL_ERROR ("Syntax is: %s <source.pcd> <target.pcd> [*]", argv[0]);
        PCL_ERROR ("[*] - multiple files can be added. The registration results of (i, i) will be registered against (i+2), etc");
        return (-1);
    }
    PCL_INFO ("Loaded %d datasets.\n", (int)data.size ());

    // Create a PCLVisualizer object
    p = new pcl::visualization::PCLVisualizer (argc, argv, "Pairwise Incremental Registration example");

    // Initialize new interactor style and set it to TrackballCamera
    vtkSmartPointer<vtkInteractorStyleSwitch> interactor_style = vtkSmartPointer<vtkInteractorStyleSwitch>::New();
    interactor_style->SetCurrentStyleToTrackballCamera ();
    // Change InteractorStyle to vtkInteractorStyleSwitch
    p->setupInteractor (p->getRenderWindow()->GetInteractor(), p->getRenderWindow(), interactor_style);

    p->createViewPort (0.0, 0, 0.5, 1.0, vp_1);
    p->createViewPort (0.5, 0, 1.0, 1.0, vp_2);

    PointCloud::Ptr cloud_WRT_firstCloud (new PointCloud);



    Eigen::Matrix4f GlobalTransform = Eigen::Matrix4f::Identity (), T_mouse_WRT_firstCloud;

    PCD source, target;

    for (size_t i = 1; i < data.size (); ++i)
    {
        source = data[i-1];
        target = data[i];

        // Add coordinate system for viewport 1
        p->addCoordinateSystem(0.4, vp_1);
        // Set view from above
        p->setCameraPosition (0, 0, 10, 0, 0, 0, 0, 0, 0);

        std::cout << "*************************************************" << endl;
        std::cout << "Type 'j' or 't' to select joystick or trackball, " << std::endl;
        std::cout << "and type 'c' or 'a' to select camera or actor. " << std::endl;
        std::cout << "Move the green cloud!" << std::endl;
        std::cout << "*************************************************" << std::endl;

        // Add visualization data
        showCloudsLeft(target, source);

        // Eigen::Affine3f ViewerPose = p->getViewerPose (vp_1);
        // std::cout << " " << std::endl;
        // std::cout << "ViewerPose: " << std::endl;
        // std::cout << ViewerPose.matrix() << std::endl;

        // std::cout << " " << std::endl;
        // std::cout << "target.f_name: " << target.f_name << std::endl;""
        // std::cout << "source.f_name: " << source.f_name << std::endl;

        // Get Cloud Actor Map(boosted::unorderedmap) that allow us to manipulate visualizer's actors
        pcl::visualization::CloudActorMapPtr cloud_actor_map = p->getCloudActorMap ();
        // Get the transformation from viewer and apply to my Cloud
        vtkSmartPointer<vtkMatrix4x4> T_target_WRT_mouse_VTK = cloud_actor_map->at(target.f_name).viewpoint_transformation_;

        Eigen::Matrix4f T_target_WTR_mouse;
        pcl::visualization::PCLVisualizer::convertToEigenMatrix (T_target_WRT_mouse_VTK, T_target_WTR_mouse);
        pcl::transformPointCloud (*target.cloud, *target.cloud, T_target_WTR_mouse);

        // std::cout << " " << std::endl;
        // std::cout << "Matrix of the transformation by mouse: " << std::endl;
        // std::cout << Ti << std::endl;
        // std::cout << " " << std::endl;
        // bool update_success = p->updatePointCloud(target.cloud, target.f_name);
        // if(!update_success)
        //   std::cout << "no cloud with the specified ID was found" << std::endl;

        // If Actor Style is set i set it to Camera Style
        interactor_style->SetCurrentStyleToTrackballCamera ();

        PointCloud::Ptr temp (new PointCloud);
        PCL_INFO ("Aligning %s (%d) with %s (%d).\n", data[i-1].f_name.c_str (), source.cloud->points.size (), data[i].f_name.c_str (), target.cloud->points.size ());
        pairAlign (source.cloud, target.cloud, temp, T_mouse_WRT_firstCloud, leaf_size, downsample);

        // transform current pair into the global transform
        pcl::copyPointCloud(*temp, *cloud_WRT_firstCloud);

        data[i].Ti = T_mouse_WRT_firstCloud * T_target_WTR_mouse;
        data[i].cloud = cloud_WRT_firstCloud;

        // write on file obtained transformation
        ofstream myfile;
        myfile.open ("transformations.txt", std::ios_base::app);
        myfile << "\n";
        myfile << "from " << pcl::getFilenameWithoutPath(data[i].f_name) << " with respect to " << pcl::getFilenameWithoutPath(argv[1]) << "\n";
        myfile << data[i].Ti;
        //myfile << "\n Transformation by hand: \n" << T_target_WTR_mouse;
        myfile << "\n";
        myfile.close();

        // save aligned pair, transformed into the first cloud's frame
        std::stringstream ss;
        ss << i << ".pcd";
        pcl::io::savePCDFile (ss.str (), *cloud_WRT_firstCloud, true);
    }
}
/* ]--- */
